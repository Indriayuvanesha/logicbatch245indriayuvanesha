--No.2
alter table dosen
alter column namadosen TYPE varchar(200)

--No.3
select c."kodemahasiswa",c."namamahasiswa",b."namamatakuliah",a."deskripsi"
from jurusankuliah as a inner join mahasiswa as c
on a."kodejurusan" = c."kodejurusan"
inner join matakuliah as b
on b."kodematakuliah" = c."kodematakuliah"
where c."kodemahasiswa" = 'MK001'
group by c."kodemahasiswa",c."namamahasiswa",b."namamatakuliah",a."deskripsi"

--No.4
select * from mahasiswa as a
inner join matakuliah as b
on a."kodematakuliah" = b."kodematakuliah"
where b."keaktifanstatus" = 'Non Aktif'

--No.5
select a.kodemahasiswa,a.namamahasiswa,a.alamatmahasiswa,b.statusorganisasi,c.nilai
from mahasiswa as a inner join nilaimahasiswa as c
on a.kodemahasiswa = c.kodemahasiswa
inner join organisasi as b
on b.kodeorganisasi = c.kodeorganisasi
where b.statusorganisasi = 'Aktif' and c.nilai > 79

--No.6
select kodematakuliah,namamatakuliah,keaktifanstatus from matakuliah
where namamatakuliah like '%n%'

--No.7
select count(b.kodemahasiswa), a.namamahasiswa from mahasiswa as a
inner join nilaimahasiswa as b
on a.kodemahasiswa = b.kodemahasiswa
group by a.namamahasiswa
having count(b.kodemahasiswa) > 1

--No.8
select a.kodemahasiswa,a.namamahasiswa,b.namamatakuliah,b.keaktifanstatus,c.deskripsi,
d.namadosen,e.penjelasan from mahasiswa as a
inner join jurusankuliah as c
on a.kodejurusan = c.kodejurusan
inner join matakuliah as b
on a.kodematakuliah = b.kodematakuliah
inner join dosen as d
on b.kodematakuliah = d.kodematakuliah
inner join fakultas as e
on d.kodefakultas = e.kodefakultas
where kodemahasiswa = 'MK001'

--No.9
create view vwmahasiswa AS
select a.kodemahasiswa,a.namamahasiswa,b.namamatakuliah,b.keaktifanstatus,
c.namadosen,d.penjelasan
from mahasiswa as a
inner join matakuliah as b
on a.kodematakuliah = b.kodematakuliah
inner join dosen as c
on b.kodematakuliah = c.kodematakuliah
inner join fakultas as d
on c.kodefakultas = d.kodefakultas

--No.10
select * from vwmahasiswa
